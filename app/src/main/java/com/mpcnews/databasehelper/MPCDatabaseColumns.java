package com.mpcnews.databasehelper;

/**
 * Created by terril on 25/8/14.
 */
public class MPCDatabaseColumns {
    public static String TABLE_NAME_ALL = "News_Listing";

    public static String _ID = "_id";
    public static String COLUMN_ID = "id";
    public static String COLUMN_TITLE = "title";
    public static String COLUMN_CAT_ID = "catid";
    public static String COLUMN_IMAGE_URL = "image_url";
    public static String COLUMN_INTROTEXT = "introtext";
    public static String COLUMN_ALIAS = "aliaas";

}
