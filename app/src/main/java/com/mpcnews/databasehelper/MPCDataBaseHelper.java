package com.mpcnews.databasehelper;

import android.content.ContentValues;

import com.mpcnews.utils.NewsUtil;

import java.util.List;

/**
 * Created by terril on 21/8/14.
 */
public interface MPCDataBaseHelper {

    public void insert(ContentValues values, String tableName);
    public List<NewsUtil> readAllData(String tableName);
    public void deleteAllData(String tableName);
    public void updateSingleColumn();
}
