package com.mpcnews.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mpcnews.R;
import com.mpcnews.activities.NewsDetailsActivity;
import com.mpcnews.activities.SplashScreenActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

/**
 * Created by terril on 8/10/14.
 */
public class GcmIntentService extends GcmListenerService {

    public int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public GcmIntentService() {
        super();
    }


    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
//        String dir = Environment.getExternalStorageDirectory()+File.separator+"MpcNews";
//        //create folder
//        File folder = new File(dir); //folder name
//        folder.mkdirs();
//
//        //create file
//        File file = new File(dir, "MpcNews.txt");
//        try {
//        FileOutputStream fOut = new FileOutputStream(file);
//        OutputStreamWriter myOutWriter =
//                new OutputStreamWriter(fOut);
//        myOutWriter.append(bundle2string(data));
//        myOutWriter.close();
//
//            fOut.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        // String messageType = gcm.getMessageType(intent);

        //  if (!data.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */

        sendNotification(data);
//            if (GoogleCloudMessaging.
//                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//                // sendNotification("Send error: " + extras.toString());
//            } else if (GoogleCloudMessaging.
//                    MESSAGE_TYPE_DELETED.equals(messageType)) {
////                sendNotification("Deleted messages on server: " +
////                        extras.toString());
//                // If it's a regular GCM message, do some work.
//            } else if (GoogleCloudMessaging.
//                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//
//
//                // Log.i("TAG", "Received: " + extras.toString());
//            }
        // }
    }

    public static String bundle2string(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = "Bundle{";
        for (String key : bundle.keySet()) {
            string += " " + key + " => " + bundle.get(key) + ";";
        }
        string += " }Bundle";
        return string;
    }

//    @Override
//    protected void onHandleIntent(Intent intent) {
//        Bundle extras = intent.getExtras();
//
//        // Release the wake lock provided by the WakefulBroadcastReceiver.
//        GcmBroadcastReceiver.completeWakefulIntent(intent);
//
//    }

    private Notification setBigPictureStyleNotification(Bundle extras) {
        Bitmap remote_picture = null;


        String title = extras.getString("title");
        String newsContent = extras.getString("introtext");
        String id = extras.getString("id");
        String alias = extras.getString("alias");
        String imageUrl = extras.getString("imageurl");

        // Create the style object with BigPictureStyle subclass.
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        // notiStyle.setBigContentTitle(title);
        notiStyle.setSummaryText(title);

        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(imageUrl).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        notiStyle.bigPicture(remote_picture);

        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("newsContent", newsContent);
        bundle.putString("id", id);
        bundle.putString("alias", alias);
        bundle.putString("imageUrl", imageUrl);
        try {
            NOTIFICATION_ID = Integer.parseInt(id);
        } catch (NumberFormatException ne) {

        }
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sounds_jubilation);
        Intent intent = new Intent(this, NewsDetailsActivity.class);
        intent.putExtra("bundle", bundle);

//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(NewsDetailsActivity.class);
//        stackBuilder.addNextIntent(intent);
        SharedPreferences prefs = this.getSharedPreferences(SplashScreenActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        boolean sound = prefs.getBoolean("sound", false);

        Intent resultIntent = new Intent(this, NewsDetailsActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(NewsDetailsActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, intent
                , PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this);
        mNotificationBuilder.setSmallIcon(getNotificationIcon());
        mNotificationBuilder.setContentTitle("MPCNews");
        mNotificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(title));
        mNotificationBuilder.setLights(0xfff86609, 100, 3000);
        mNotificationBuilder.setVibrate(new long[]{500, 500, 500, 500, 500});
        if(!sound){
            mNotificationBuilder.setSound(alarmSound);
        }
        mNotificationBuilder.setPriority(Notification.PRIORITY_DEFAULT);
        mNotificationBuilder.setContentText(title).setAutoCancel(true);
        mNotificationBuilder.setStyle(notiStyle).build();
        mNotificationBuilder.setContentIntent(contentIntent);
        return mNotificationBuilder.build();
    }

    private void sendNotification(Bundle extras) {
//
//        String title = extras.getString("title");
//        String newsContent = extras.getString("introtext");
        String id = extras.getString("id");
//        String alias = extras.getString("alias");
//        String imageUrl = extras.getString("imageurl");
////
////        try {
////            File myFile = new File("/sdcard/mpctext.txt");
////            myFile.createNewFile();
////            FileOutputStream fOut = new FileOutputStream(myFile);
////            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
////            myOutWriter.write(title+"\n"+imageUrl);
////            myOutWriter.close();
////            fOut.close();
////        }catch(Exception e){
////
////        }
//
//        Bundle bundle = new Bundle();
//        bundle.putString("title", title);
//        bundle.putString("newsContent", newsContent);
//        bundle.putString("id", id);
//        bundle.putString("alias", alias);
//        bundle.putString("imageUrl", imageUrl);
        try {
            NOTIFICATION_ID = Integer.parseInt(id);
        } catch (NumberFormatException ne) {

        }
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sounds_jubilation);
//        Intent intent = new Intent(this, NewsDetailsActivity.class);
//        intent.putExtra("bundle", bundle);

//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(NewsDetailsActivity.class);
//        stackBuilder.addNextIntent(intent);


//        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, intent
//                , PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(getNotificationIcon())
//                        .setContentTitle("MPCNews")
//                        .setStyle(new NotificationCompat.BigTextStyle()
//                                .bigText(title))
//                        .setLights(0xfff86609, 100, 3000)
//                        .setVibrate(new long[]{500, 500, 500, 500, 500})
//                        .setSound(alarmSound)
//                        .setPriority(Notification.PRIORITY_DEFAULT)
//                        .setContentText(title).setAutoCancel(true);
//
//        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, setBigPictureStyleNotification(extras));
        //     mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.drawable.app_icon_black : R.drawable.app_icon;
    }
}
