package com.mpcnews.utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by terril on 27/9/14.
 */
public class NewsUtil implements Parcelable {


    private String id;
    private String title;
    private String alias;
    private String title_alias;
    private String introtext;
    private String fulltext;
    private String state;
    private String sectionid;
    private String mask;
    private String catid;
    private String created;
    private String created_by;
    private String modified;
    private String modified_by;
    private String checked_out;
    private String checked_out_time;
    private String publish_up;
    private String publish_down;
    private String images;
    private ArrayList<String> imageUrl;
    private String urls;
    private String attribs;
    private String version;
    private String parentid;
    private String ordering;
    private String access;
    // private ArrayList<String> paragraphs;
    private String hits;
    private String header;
    // ArrayList<String> newsItems = new ArrayList<String>()

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    public String getHits() {
        return hits;
    }

    public void setHits(String hits) {
        this.hits = hits;
    }

    public String getIntrotext() {
        return introtext;
    }

    public void setIntrotext(String introtext) {
        this.introtext = introtext;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public ArrayList<String> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(ArrayList<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(alias);
        dest.writeString(introtext);
        dest.writeString(catid);
        dest.writeString(images);
        dest.writeString(urls);
        dest.writeString(hits);
        dest.writeString(header);
   //     dest.writeStringList(imageUrl);
    }

    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public NewsUtil createFromParcel(Parcel in) {

                    return new NewsUtil(in);
                }

                public NewsUtil[] newArray(int size) {
                    return new NewsUtil[size];
                }
            };

    public NewsUtil(){

    }
    NewsUtil(Parcel in) {
        id = in.readString();
        title = in.readString();
        alias = in.readString();
        introtext = in.readString();
        catid = in.readString();
        images = in.readString();
        urls = in.readString();
        hits = in.readString();
        header = in.readString();
      //  in.readStringList(imageUrl);
    }
}
