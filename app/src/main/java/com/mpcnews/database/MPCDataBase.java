package com.mpcnews.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mpcnews.databasehelper.MPCDataBaseHelper;
import com.mpcnews.databasehelper.MPCDatabaseColumns;
import com.mpcnews.utils.NewsUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by terril on 21/8/14.
 */
public class MPCDataBase extends SQLiteOpenHelper implements MPCDataBaseHelper {
    private static final String DATABASE_NAME = "MPCNews";
    private static final int DATABASE_VERSION = 3;

    private static final String TEXT_TYPE_INT = " INTEGER";
    private static final String TEXT_TYPE_STRING = " TEXT";
    private static final String TEXT_TYPE_REAL = " REAL";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ALL =
            "CREATE TABLE " + MPCDatabaseColumns.TABLE_NAME_ALL + " (" +
                    MPCDatabaseColumns._ID + " INTEGER PRIMARY KEY," +
                    MPCDatabaseColumns.COLUMN_ID + TEXT_TYPE_STRING + COMMA_SEP +
                    MPCDatabaseColumns.COLUMN_TITLE + TEXT_TYPE_STRING + COMMA_SEP +
                    MPCDatabaseColumns.COLUMN_ALIAS + TEXT_TYPE_STRING + COMMA_SEP +
                    MPCDatabaseColumns.COLUMN_IMAGE_URL + TEXT_TYPE_STRING + COMMA_SEP +
                    MPCDatabaseColumns.COLUMN_CAT_ID + TEXT_TYPE_STRING + COMMA_SEP +
                    MPCDatabaseColumns.COLUMN_INTROTEXT + TEXT_TYPE_STRING +
                    " )";


    private static final String SQL_DELETE_ALL =
            "DROP TABLE IF EXISTS " + MPCDatabaseColumns.TABLE_NAME_ALL;

    public MPCDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public MPCDataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ALL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL(SQL_DELETE_ALL);
        onCreate(sqLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void insert(ContentValues values, String tableName) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(tableName, null, values);
    }

    @Override
    public List<NewsUtil> readAllData(String tableName) {
        List<NewsUtil> issueList = new ArrayList<NewsUtil>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + tableName;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                NewsUtil issueUtil = new NewsUtil();
                issueUtil.setId(cursor.getString(1));
                issueUtil.setTitle(cursor.getString(2));
                issueUtil.setAlias(cursor.getString(3));
                issueUtil.setImages(cursor.getString(4));
                issueUtil.setCatid(cursor.getString(5));
                issueUtil.setIntrotext(cursor.getString(6));
                // Adding contact to list
                issueList.add(issueUtil);
            } while (cursor.moveToNext());
        }
        return issueList;
    }

    @Override
    public void deleteAllData(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + tableName);
    }

    @Override
    public void updateSingleColumn() {

    }
}
