package com.mpcnews.database;

import android.content.Context;

import com.mpcnews.databasehelper.MPCDataBaseHelper;


/**
 * Created by terril on 21/8/14.
 */
public class MPCDataBaseInstanceGeneration {

    private static MPCDataBase instance = null;

    private MPCDataBaseInstanceGeneration() {
        //  super(decoratedShape);
    }

    public static MPCDataBaseHelper getDbInstance(Context context) {
        if (instance == null) {
            instance = new MPCDataBase(context);
        }
        return instance;
    }


}
