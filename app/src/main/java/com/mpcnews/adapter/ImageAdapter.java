package com.mpcnews.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.android.volley.toolbox.NetworkImageView;
import com.mpcnews.R;
import com.mpcnews.helpers.VolleyHelper;

import java.util.ArrayList;

/**
 * Created by terril1 on 22/12/15.
 */
public class ImageAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> imageList;

    public ImageAdapter(Context context, ArrayList<String> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public String getItem(int position) {
        return imageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderImage holder;
        if (convertView == null) {
            holder = new ViewHolderImage();
            convertView = LayoutInflater.from(context).inflate(R.layout.view_imageas, parent, false);
            holder.viewImage = (NetworkImageView) convertView.findViewById(R.id.viewImage);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderImage) convertView.getTag();
        }

        holder.viewImage.setImageUrl(imageList.get(position), VolleyHelper.getInstance(context).getImageLoader());
        return convertView;
    }

    class ViewHolderImage {

        NetworkImageView viewImage;
    }
}
