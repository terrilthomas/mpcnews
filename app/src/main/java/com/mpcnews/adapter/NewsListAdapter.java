package com.mpcnews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.mpcnews.R;
import com.mpcnews.helpers.CallBackIfBreakingNews;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.VolleyHelper;
import com.mpcnews.utils.NewsUtil;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by terril on 27/9/14.
 */
public class NewsListAdapter extends BaseAdapter implements
        StickyListHeadersAdapter, SectionIndexer {
    Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<NewsUtil> util;
    private String[] headerName;
    private int[] sectionIndices;
    String[] sectionHeader;
    CallBackIfBreakingNews callBackIfBreakingNews;

    public NewsListAdapter(Context context, ArrayList<NewsUtil> utils,CallBackIfBreakingNews callBackIfBreakingNews) {
        mContext = context;
        util = utils;
        this.callBackIfBreakingNews = callBackIfBreakingNews;
        headerName = context.getResources().getStringArray(R.array.headerName);
        mInflater = LayoutInflater.from(context);
        sectionIndices = getSectionIndices();
        sectionHeader = getSectionHeaders();
    }

    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        int lastFirstChar = Integer.parseInt(util.get(0).getCatid());
        sectionIndices.add(0);
        for (int i = 1; i < util.size(); i++) {
            if (Integer.parseInt(util.get(i).getCatid()) != lastFirstChar) {
                lastFirstChar = Integer.parseInt(util.get(i).getCatid());
                sectionIndices.add(i);
            }
        }

        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    private String[] getSectionHeaders() {

        String[] letters = new String[sectionIndices.length];
        for (int i = 0; i < sectionIndices.length; i++) {
            letters[i] =
                    util.get(sectionIndices[i]).getCatid();
            //headerName[sectionIndices[i]];
        }

        return letters;
    }

    @Override
    public int getCount() {
        return util.size();
    }

    @Override
    public Object getItem(int position) {
        return util.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.view_news_items, null, false);
            holder.lblNewTitle = (TextView) convertView.findViewById(R.id.lblNewTitle);
            holder.viewImage = (NetworkImageView) convertView.findViewById(R.id.viewImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String imageUrl = util.get(position).getImages();
//        if(!imageInstance.equals("")) {
//            holder.viewImage.setVisibility(View.VISIBLE);
//            String imageUrl = Commons.URL + imageInstance;
//            holder.viewImage.setImageUrl(imageUrl, VolleyHelper.getInstance(mContext).getImageLoader());
//        }else{
//            holder.viewImage.setVisibility(View.GONE);
//        }
        holder.lblNewTitle.setText(util.get(position).getTitle());
        holder.viewImage.setImageUrl(imageUrl, VolleyHelper.getInstance(mContext).getImageLoader());

        return convertView;
    }

    @Override
    public Object[] getSections() {
        return sectionHeader;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        if (sectionIndices.length == 0) {
            return 0;
        }
        if (sectionIndex >= sectionIndices.length) {
            sectionIndex = sectionIndices.length - 1;
        } else if (sectionIndex < 0) {
            sectionIndex = 0;
        }
        return sectionIndices[sectionIndex];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < sectionIndices.length; i++) {
            if (position < sectionIndices[i]) {
                return i - 1;
            }
        }
        return sectionIndices.length - 1;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.view_header, parent, false);
            holder.lblHeader = (TextView) convertView.findViewById(R.id.lblHeader);
            holder.lblMore = (TextView) convertView.findViewById(R.id.lblMore);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        // set header text as first char in name
        //  CharSequence headerChar = headerName[position].subSequence(0, 1);
        int catid = Integer.parseInt(util.get(position).getCatid());
        String header = "";
        if (catid == 22) {
            header = headerName[1];//
            holder.lblMore.setVisibility(View.GONE);
        } else if (catid == 1) {
            header = headerName[2];
            holder.lblMore.setVisibility(View.VISIBLE);
        } else if (catid == 2) {
            header = headerName[3];
            holder.lblMore.setVisibility(View.VISIBLE);
        } else if (catid == 3) {
            header = headerName[4];
            holder.lblMore.setVisibility(View.VISIBLE);
        } else if (catid == 12) {
            header = headerName[5];
            holder.lblMore.setVisibility(View.VISIBLE);
        } else if (catid == 11) {
            header = headerName[6];
            holder.lblMore.setVisibility(View.VISIBLE);
        } else if (catid == 13) {
            header = headerName[7];
            holder.lblMore.setVisibility(View.VISIBLE);
        } else {
            header = headerName[0];
           // callBackIfBreakingNews.itsABreakingNews(util.get(0).getTitle());
        }


        holder.lblHeader.setText(header);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        // Log.e("TAG ", headerName[position].subSequence(0, 1).charAt(0) + " : Header ID");
        int headerPosition = Integer.parseInt(util.get(position).getCatid());
        return headerPosition;
    }

    class ViewHolder {
        NetworkImageView viewImage;
        TextView lblNewTitle;
    }

    class HeaderViewHolder {
        TextView lblHeader;
        TextView lblMore;
    }
}
