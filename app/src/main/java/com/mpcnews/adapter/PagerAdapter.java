package com.mpcnews.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.mpcnews.fragments.ScreenSlidePageFragment;

import java.util.ArrayList;

/**
 * Created by terril on 3/11/14.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<String> imageSet;

    public PagerAdapter(FragmentManager fm, ArrayList<String> imageSet) {
        super(fm);
        this.imageSet = imageSet;
    }

    @Override
    public Fragment getItem(int position) {
        String imageUrl = imageSet.get(position);
        Bundle bundle = new Bundle();
       // Log.i("TAG", "" + position + "" + imageSet.size());
        bundle.putString("imageUrl", imageUrl);
        if (imageSet.size() > 1)
            bundle.putBoolean("nextItem", true);
        if (position == imageSet.size() - 1)
            bundle.putBoolean("nextItem", false);
        Fragment fragment = new ScreenSlidePageFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {
        return imageSet.size();
    }
}

