package com.mpcnews.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mpcnews.fragments.NewsScreenSlideFragment;
import com.mpcnews.utils.NewsUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by terril on 24/11/14.
 */
public class NewsSlideAdapter extends FragmentStatePagerAdapter {
    ArrayList<NewsUtil> newsUtils;
    ArrayList<String> images;

    public NewsSlideAdapter(FragmentManager fm, ArrayList<NewsUtil> newsUtils, ArrayList<String> images) {
        super(fm);
        this.newsUtils = newsUtils;
        this.images = images;
    }

    @Override
    public Fragment getItem(int i) {
        String newsHeader = newsUtils.get(i).getTitle();
        String newsContent = newsUtils.get(i).getIntrotext();
        String alias = newsUtils.get(i).getAlias();
        String id = newsUtils.get(i).getId();
        String imageUrl = newsUtils.get(i).getImages();
       // ArrayList<String> images = (ArrayList) newsUtils.get(i).getImageUrl();
        Bundle bundle = new Bundle();
        bundle.putString("newsHeader", newsHeader);
        bundle.putString("newsContent", newsContent);
        bundle.putString("alias", alias);
        bundle.putString("id", id);
        bundle.putString("imageUrl", imageUrl);
        bundle.putStringArrayList("newsImageUrl", images);
        Fragment fragment = new NewsScreenSlideFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {
        return newsUtils.size();
    }
}
