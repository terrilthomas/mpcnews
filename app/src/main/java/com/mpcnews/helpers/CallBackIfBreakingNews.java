package com.mpcnews.helpers;

/**
 * Created by terril on 2/2/15.
 */
public interface CallBackIfBreakingNews {

    public void itsABreakingNews(String breakingNews);
}
