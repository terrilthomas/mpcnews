package com.mpcnews.helpers;

/**
 * Created by terril on 23/9/14.
 */
public class Commons {
    public static String URL = "http://mpcnews.in/";
    public static String SHOW_URL = "http://mpcnews.in/shownews.php?id=";
    public static String BASE_URL = "http://mpcnews.in/webserv.php?";
    public static String NOTIFICATION_URL = "http://mpcnews.in/ns.php?r=";//Notification Settings

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String PROPERTY_REG_ID = "registration_id";
}
//http://mpcnews.in/getmorenews.php?catid=11