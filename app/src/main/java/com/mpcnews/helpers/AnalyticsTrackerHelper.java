package com.mpcnews.helpers;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.analytics.Tracker;
import com.mpcnews.application.MpcNewsApplication;

/**
 * Created by terril on 14/1/15.
 */
public  class AnalyticsTrackerHelper {

     public static void trackUserCurrentPage(Activity activity, String screenName){

         Tracker t = ((MpcNewsApplication) activity.getApplication()).getTracker(MpcNewsApplication.TrackerName.APP_TRACKER);
         t.enableExceptionReporting(true);
         t.setScreenName(screenName);
     }
}
