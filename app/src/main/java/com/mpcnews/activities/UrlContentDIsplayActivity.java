package com.mpcnews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.mpcnews.R;
import com.mpcnews.adapter.NewsListAdapter;
import com.mpcnews.adapter.PagerAdapter;
import com.mpcnews.databasehelper.MPCDatabaseColumns;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.NetworkHelper;
import com.mpcnews.helpers.VolleyHelper;
import com.mpcnews.utils.NewsUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by terril on 13/10/14.
 */
public class UrlContentDIsplayActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        final Intent intent = getIntent();
        // final String action = intent.getAction();
        String dataUrl = intent.getDataString();
        try {
            String[] url = dataUrl.split("/");
            String[] newUrl = url[7].split("-");
            initializeView(newUrl[0]);
        } catch (ArrayIndexOutOfBoundsException ae) {
            WebView web = (WebView) findViewById(R.id.webView);
            web.setVisibility(View.VISIBLE);
            web.loadUrl("http://mpcnews.in");

        }


    }

    private void initializeView(String id) {
        TextView lblNewsHeader = (TextView) findViewById(R.id.lblNewsHeader);
        TextView lblNewsContent = (TextView) findViewById(R.id.lblNewsContent);
        //NetworkImageView imvNewsImage = (NetworkImageView) findViewById(R.id.imvNewsImage);
        NetworkImageView pager = (NetworkImageView) findViewById(R.id.imvNewsImage);

        if (NetworkHelper.isOnline(this)) {
            setProgressBarIndeterminateVisibility(true);
            serviceCallForNewNews(id, lblNewsHeader, lblNewsContent, pager);
        } else {
            NetworkHelper.noNetworkToast(this);
        }
    }

    private void serviceCallForNewNews(String id, final TextView lblNewsHeader, final TextView lblNewsContent, final NetworkImageView imvNewsImageHeader) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, Commons.SHOW_URL + id, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e("TAG", response.toString());
                        setProgressBarIndeterminateVisibility(false);
                        if (response != null) {
                            try {
                                String status = response.getString("status");
                                if (status.equals("1")) {
                                    ArrayList<String> images = new ArrayList<String>();
                                    JSONArray jsonArray = response.getJSONArray("details");
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    String title = jsonObject.getString("title");
                                    String newsImage = jsonObject.getString("newsimage");
                                    StringBuilder builder = new StringBuilder();
//                                String[] content = newsContent.split("<p style=\\\"text-align: justify;>\\\"");
//                                for (int count = 0; count < content.length; count++) {
//                                    Log.e("TAG", content[count]);
//                                }

//                                String stripped = newsContent.replaceAll("<[^>]*>", "");
//                                System.out.println(stripped);
                                    JSONArray array = jsonObject.getJSONArray("paragraphs");
                                    for (int paracount = 0; paracount < array.length(); paracount++) {
                                        // Log.e("TAG", "Loop count :" + paracount);
                                        if (!array.getString(paracount).equals(""))
                                            builder.append(array.get(paracount)).append("\n");
                                        //Log.e("TAG", "Incoming Text :" + jsonArray.getString(paracount));
                                    }
                                    images.add(0, newsImage);
                                    JSONArray imageArray = jsonObject.getJSONArray("image_url");
                                    for (int imagecount = 0; imagecount < imageArray.length(); imagecount++) {
                                        // Log.e("TAG", "Loop count :" + paracount);
                                        if (!imageArray.getString(imagecount).equals("")) {
                                            images.add(imagecount + 1, imageArray.getString(imagecount));

                                        }
                                        //Log.e("TAG", "Incoming Text :" + jsonArray.getString(paracount));
                                    }

                                    // util.setParagraphs(para);
                                    lblNewsHeader.setText(title);
                                    imvNewsImageHeader.setImageUrl(newsImage, VolleyHelper.getInstance(UrlContentDIsplayActivity.this).getImageLoader());
                                    lblNewsContent.setText(builder.toString());
                                    lblNewsContent.setMovementMethod(new ScrollingMovementMethod());
                                    // imvNewsImage.setImageUrl(newsImage, VolleyHelper.getInstance(UrlContentDIsplayActivity.this).getImageLoader());
//                                    PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager(), images);
//                                    pager.setAdapter(mPagerAdapter);

                                } else {
                                    Toast.makeText(UrlContentDIsplayActivity.this, "The news content has been removed", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Log.e("TAG", error.toString());
                        setProgressBarIndeterminateVisibility(false);

                    }


                });

// Access the RequestQueue through your singleton class.
        VolleyHelper.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(this, getLocalClassName());
    }
}
