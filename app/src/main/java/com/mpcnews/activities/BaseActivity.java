package com.mpcnews.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by terril on 17/9/14.
 */
public class BaseActivity extends AppCompatActivity {

    protected void initializeActionBar(boolean homeEnable,boolean titleEnable){
        getSupportActionBar().setDisplayHomeAsUpEnabled(homeEnable);
        getSupportActionBar().setDisplayShowTitleEnabled(titleEnable);
    }

    public void loadFragment(Fragment fragment, Bundle bundle, int container) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragment.setArguments(bundle);
        fragmentTransaction.replace(container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void loadActivity(Context context, Class<?> classEvent, Bundle bundle, int flag) {
        Intent intent = new Intent();
        if (flag != 0)
            intent.addFlags(flag);
        intent.setClass(context, classEvent);
        intent.putExtra("bundle", bundle);
        startActivity(intent);

    }
}
