package com.mpcnews.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mpcnews.R;
import com.mpcnews.adapter.NewsListAdapter;
import com.mpcnews.databasehelper.MPCDatabaseColumns;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.VolleyHelper;
import com.mpcnews.utils.NewsUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by terrilthomas on 04/07/15.
 */
public class MoreNewsActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    ArrayList<NewsUtil> utilArrayList = new ArrayList<NewsUtil>();
    private ListView lstMoreNews;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_news);
        initializeActionBar(true, true);

        lstMoreNews = (ListView) findViewById(R.id.lstMoreNews);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        lstMoreNews.setOnItemClickListener(this);
        String catId = getIntent().getBundleExtra("bundle").getString("catId");

        serviceCallForMorenewsFeed(catId);
    }

    private void serviceCallForMorenewsFeed(String catId) {

        // Log.e("URL",Commons.BASE_URL + regId + "&e="+possibleEmail);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Commons.URL + "getmorenews.php?" + "catid=" + catId, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        setProgressBarIndeterminateVisibility(false);
                        progressBar.setVisibility(View.GONE);
                        if (response != null) {
                            for (int count = 0; count < response.length(); count++) {
                                ArrayList<String> images = new ArrayList<String>();
                                try {
                                    NewsUtil util = new NewsUtil();
                                    JSONObject object = response.getJSONObject(count);

                                    util.setId(object.getString("id"));
                                    util.setTitle(object.getString("title"));
                                    util.setCatid(object.getString("catid"));
                                    util.setImages(object.getString("newsimage"));
                                    StringBuilder builder = new StringBuilder();
                                    JSONArray jsonArray = object.getJSONArray("paragraphs");
                                    for (int paracount = 0; paracount < jsonArray.length(); paracount++) {
                                        if (!jsonArray.getString(paracount).equals("")) {
                                            builder.append(jsonArray.getString(paracount)).append("\n");
                                        }

                                    }
                                    JSONArray imageArray = object.getJSONArray("image_url");
                                    for (int imagecount = 0; imagecount < imageArray.length(); imagecount++) {

                                        //  if (!imageArray.getString(imagecount).equals("")) {
                                        images.add(Commons.URL + imageArray.getString(imagecount));

                                        // }
                                    }

                                    util.setImageUrl(images);
                                    Log.e("TAG", "Images Under ImageUrl : " + util.getImageUrl());
                                    util.setIntrotext(builder.toString());//object.getString("newintrotext")
                                    //  util.setParagraphs(para);
                                    util.setAlias(object.getString("alias"));
                                    utilArrayList.add(util);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            NewsListAdapter adapter = new NewsListAdapter(MoreNewsActivity.this, utilArrayList, null);
                            lstMoreNews.setAdapter(adapter);

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setProgressBarIndeterminateVisibility(false);
                        Log.e("TAG", "Error : " + error.networkResponse);
                        //Toast.makeText(NewsListActivity.this, "No Data Obtained", Toast.LENGTH_LONG).show();
                    }


                });

// Access the RequestQueue through your singleton class.
        VolleyHelper.getInstance(this).addToRequestQueue(jsonArrayRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(this, getLocalClassName());
    }

    @Override
    protected void onStop() {
        super.onStop();
        VolleyHelper.getInstance(this).getRequestQueue().getCache().clear();
    }

    //ArrayList<NewsUtil> newNews;
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ArrayList<String> images = null;
        String catId = utilArrayList.get(position).getCatid();
        for (int count = 0; count < utilArrayList.size(); count++) {
            if (catId.equals(utilArrayList.get(count).getCatid())) {
                images = utilArrayList.get(count).getImageUrl();
            }
        }
        Bundle bundle = new Bundle();
        bundle.putString("catId", utilArrayList.get(position).getCatid());
        bundle.putInt("position", position);
        bundle.putStringArrayList("imageArrayList", images);
        bundle.putBoolean("moreNews", true);
        bundle.putParcelableArrayList("fullListData", utilArrayList);
        loadActivity(MoreNewsActivity.this, NewsInSeriesActivity.class, bundle, 0);
    }

}
