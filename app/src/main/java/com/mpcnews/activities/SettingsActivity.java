package com.mpcnews.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mpcnews.R;
import com.mpcnews.fragments.AboutFragment;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by terril on 13/10/14.
 */
public class SettingsActivity extends BaseActivity {
    String registrationId;
    SharedPreferences.Editor editor;
    private boolean state,soundVolume;
    private ToggleButton toggleButton, toggleButtonSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initializeActionBar(true, true);
        SharedPreferences prefs = getSharedPreferences(SplashScreenActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
        editor = prefs.edit();
        registrationId = prefs.getString("registration_id", "");
        state = prefs.getBoolean("isChecked", true);
        soundVolume = prefs.getBoolean("sound", true);
        TextView lblAboutUs = (TextView) findViewById(R.id.lblAboutUs);
        TextView lblRatetheapp = (TextView) findViewById(R.id.lblRateTheApp);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        toggleButtonSound = (ToggleButton) findViewById(R.id.toggleButtonSound);
        toggleButton.setChecked(state);
        toggleButtonSound.setChecked(soundVolume);
        toggleButton.setOnCheckedChangeListener(getCheckChanged());
        toggleButtonSound.setOnCheckedChangeListener(getCheckChanged());

        lblAboutUs.setOnClickListener(initializeListner());
        lblRatetheapp.setOnClickListener(initializeListner());
    }

    private ToggleButton.OnCheckedChangeListener getCheckChanged() {
        ToggleButton.OnCheckedChangeListener listner = new ToggleButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (buttonView == toggleButton) {
                    editor.putBoolean("isChecked", isChecked);
                    editor.commit();
                    if (isChecked) {
//                    Log.e("TAG", "isChecked");
                        statusNotification(1);
                    } else {
                        statusNotification(0);
//                    editor.putBoolean("isChecked", false);
//                    Log.e("TAG", "isChecked false");
                    }
                } else {
                    if (isChecked) {
                       // Log.e("TAG","ON");
                        editor.putBoolean("sound", true);
                        editor.commit();

                       // AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//                        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
                      //  audio.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, AudioManager.FLAG_PLAY_SOUND);
                    } else {
                       // Log.e("TAG","OFF");
                        editor.putBoolean("sound", false);
                        editor.commit();

                        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        //int currentVolume = audio.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
                        audio.setStreamVolume(AudioManager.STREAM_NOTIFICATION,10, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                    }
                }
            }
        };
        return listner;
    }

    private View.OnClickListener initializeListner() {
        View.OnClickListener listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.lblAboutUs:
                        loadFragment(new AboutFragment(), null, R.id.frameContainer);
                        break;
                    case R.id.lblRateTheApp:
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.mpcnews"));
                        startActivity(intent);
                        break;
                }

            }
        };

        return listner;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(this, getLocalClassName());
    }

    private void statusNotification(int state) {
//        Log.e("TAG",Commons.NOTIFICATION_URL + registrationId + "&s=" + state);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, Commons.NOTIFICATION_URL + registrationId + "&s=" + state, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setProgressBarIndeterminateVisibility(false);
                        if (response != null) {
                            try {
                                //  Log.e("TAG", response.toString());
                                String status = response.getString("status");
                                if (status.equals("1")) {
                                    Toast.makeText(SettingsActivity.this, "Updated for Notification", Toast.LENGTH_SHORT).show();
                                } else {
                                    // Toast.makeText(SettingsActivity.this, "The news content has been removed", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setProgressBarIndeterminateVisibility(false);

                    }


                });

// Access the RequestQueue through your singleton class.
        VolleyHelper.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

}
