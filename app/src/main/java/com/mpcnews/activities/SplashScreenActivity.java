package com.mpcnews.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mpcnews.R;
import com.mpcnews.helpers.Commons;
import com.mpcnews.service.RegistrationIntentService;

import java.io.IOException;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by terril on 8/10/14.
 */
public class SplashScreenActivity extends Activity {

//    public static final String EXTRA_MESSAGE = "message";
//    public static final String PROPERTY_REG_ID = "registration_id";
//    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    String SENDER_ID = "460905649267";
    Context context;
    GoogleCloudMessaging gcm;
  //  private String regid;
    /**
     * The count.
     */
   // int count = 0;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    /**
     * The my timer.
     */
 //   Timer myTimer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = getApplicationContext();
//        myTimer.schedule(new TimerTask() {
//
//            @Override
//            public void run() {
//                if (count == 1)
//                    proceedToLogin();
//                count++;
//            }
//        }, 0, 3000);
        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
//        if (checkPlayServices()) {
//            gcm = GoogleCloudMessaging.getInstance(this);
//            regid = getRegistrationId(context);
//            Log.e("TAG", regid);
//            if (regid.isEmpty()) {
//                 registerInBackground();
//                Intent intent = new Intent(this, RegistrationIntentService.class);
//                startService(intent);
//            }
//        } else {
//            Log.i("TAG", "No valid Google Play Services APK found.");
//        }
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(Commons.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                   // Log.e("Message Received","Tango inside sentToken");
                    proceedToLogin();
                } else {
                    //Log.e("New Message Received","Tango inside else");
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Commons.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onStop() {
//
//        Log.e("Called" , "onstop called");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onStop();
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        Log.e("Called", "ondestroy called");
//
//    }

    public void proceedToLogin() {
        Intent callLoginActivity = new Intent(SplashScreenActivity.this,
                NewsListActivity.class);
        callLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        callLoginActivity.putExtra("regid", PreferenceManager.getDefaultSharedPreferences(this).getString(Commons.PROPERTY_REG_ID,"0"));
        startActivity(callLoginActivity);
        finish();
        //myTimer.cancel();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

//    private String getRegistrationId(Context context) {
//        final SharedPreferences prefs = getGCMPreferences(context);
//        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
//        if (registrationId.isEmpty()) {
//            Log.i("TAG", "Registration not found.");
//            return "";
//        }
//        // Check if app was updated; if so, it must clear the registration ID
//        // since the existing regID is not guaranteed to work with the new
//        // app version.
//        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
//        int currentVersion = getAppVersion(context);
//        if (registeredVersion != currentVersion) {
//            Log.i("TAG", "App version changed.");
//            return "";
//        }
//        return registrationId;
//    }

//    private SharedPreferences getGCMPreferences(Context context) {
//        // This sample app persists the registration ID in shared preferences, but
//        // how you store the regID in your app is up to you.
//        return getSharedPreferences(SplashScreenActivity.class.getSimpleName(),
//                Context.MODE_PRIVATE);
//    }
//
//    private static int getAppVersion(Context context) {
//        try {
//            PackageInfo packageInfo = context.getPackageManager()
//                    .getPackageInfo(context.getPackageName(), 0);
//            return packageInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            // should never happen
//            throw new RuntimeException("Could not get package name: " + e);
//        }
//    }

//    private void registerInBackground() {
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... params) {
//                String msg = "";
//                try {
//                    if (gcm == null) {
//                        gcm = GoogleCloudMessaging.getInstance(context);
//                    }
//                    regid = gcm.register(SENDER_ID);
//                    msg = "Device registered, registration ID=" + regid;
//
//                    // You should send the registration ID to your server over HTTP,
//                    // so it can use GCM/HTTP or CCS to send messages to your app.
//                    // The request to your server should be authenticated if your app
//                    // is using accounts.
//                    // sendRegistrationIdToBackend();
//
//                    // For this demo: we don't need to send it because the device
//                    // will send upstream messages to a server that echo back the
//                    // message using the 'from' address in the message.
//
//                    // Persist the regID - no need to register again.
//                    storeRegistrationId(context, regid);
//                } catch (IOException ex) {
//                    msg = "Error :" + ex.getMessage();
//                    // If there is an error, don't just keep trying to register.
//                    // Require the user to click a button again, or perform
//                    // exponential back-off.
//                }
//                return msg;
//            }
//
//
//        }.execute(null, null, null);
//
//    }

//    private void sendRegistrationIdToBackend() {
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("gcm_regid", regid);
//        JsonObjectRequest req = new JsonObjectRequest(Commons.URL + "notification", new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            VolleyLog.v("Response:%n %s", response.toString(4));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.e("Error: ", error.getMessage());
//            }
//        });
//
//
//    }

//    private void storeRegistrationId(Context context, String regId) {
//        final SharedPreferences prefs = getGCMPreferences(context);
//        int appVersion = getAppVersion(context);
//        //Log.i("TAG", "Saving regId on app version " + appVersion);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString(PROPERTY_REG_ID, regId);
//        editor.putInt(PROPERTY_APP_VERSION, appVersion);
//        editor.commit();
//    }
}

