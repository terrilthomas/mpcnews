package com.mpcnews.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentValues;
import android.content.res.Configuration;
import android.os.Bundle;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.mpcnews.R;
import com.mpcnews.adapter.NewsListAdapter;
import com.mpcnews.database.MPCDataBaseInstanceGeneration;
import com.mpcnews.databasehelper.MPCDataBaseHelper;
import com.mpcnews.databasehelper.MPCDatabaseColumns;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.CallBackIfBreakingNews;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.NetworkHelper;
import com.mpcnews.helpers.VolleyHelper;
import com.mpcnews.utils.NewsUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.Inflater;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class NewsListActivity extends BaseActivity implements
        AdapterView.OnItemClickListener, StickyListHeadersListView.OnHeaderClickListener,
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
        StickyListHeadersListView.OnStickyHeaderChangedListener, CallBackIfBreakingNews {

    private static final int MY_SOCKET_TIMEOUT_MS = 6000;
    String regId;
    private MPCDataBaseHelper helper;
    private StickyListHeadersListView viewList;
    private boolean offline = false;
    private String possibleEmail;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        regId = bundle.getString("regid");
        getUserEmail();
        //  Log.e("TAG", regId + "");
        setContentView(R.layout.activity_news_list);
        initializeActionBar(false, true);

        helper = MPCDataBaseInstanceGeneration.getDbInstance(getBaseContext());
        initializeView();

        FloatingActionButton action_setting = (FloatingActionButton) findViewById(R.id.action_setting);
        action_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadActivity(NewsListActivity.this, SettingsActivity.class, null, 0);
            }
        });

        FloatingActionButton action_refresh = (FloatingActionButton) findViewById(R.id.action_refresh);
        action_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkHelper.isOnline(NewsListActivity.this)) {
                    setProgressBarIndeterminateVisibility(true);
                    serviceCallFornewsFeed(viewList);
                } else {
                    Toast.makeText(NewsListActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    offlineImageOnMenuBar(true);
                }
            }
        });
    }

    private void getUserEmail() {

        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(this).getAccounts();

        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;

            }
        }
        // Log.e("TAG" , possibleEmail);
    }

    private void initializeView() {
        viewList = (StickyListHeadersListView) findViewById(R.id.viewList);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        viewList.setOnItemClickListener(this);
        viewList.setOnHeaderClickListener(this);
        viewList.setOnStickyHeaderChangedListener(this);
        viewList.setOnStickyHeaderOffsetChangedListener(this);
        viewList.setAreHeadersSticky(true);
        if (NetworkHelper.isOnline(this)) {
            setProgressBarIndeterminateVisibility(true);
            serviceCallFornewsFeed(viewList);
        } else {
            progressBar.setVisibility(View.GONE);
            utilArrayList = (ArrayList) helper.readAllData(MPCDatabaseColumns.TABLE_NAME_ALL);
            NewsListAdapter adapter = new NewsListAdapter(NewsListActivity.this, utilArrayList, NewsListActivity.this);
            viewList.setAdapter(adapter);
            offlineImageOnMenuBar(true);
            //  utilArrayList = (ArrayList) helper.readAllData(MPCDatabaseColumns.TABLE_NAME_ALL);
            if (utilArrayList.size() <= 0)
                viewList.setEmptyView(findViewById(R.id.empty_view));
            //NetworkHelper.noNetworkToast(this);
        }
    }

    private void OfflineDataParsing() {
        NewsUtil util = new NewsUtil();
        ArrayList<NewsUtil> newsUtil = (ArrayList) helper.readAllData(MPCDatabaseColumns.TABLE_NAME_ALL);
        for (NewsUtil newsItem : newsUtil) {
            util.setId(newsItem.getId());
            util.setTitle(newsItem.getTitle());
            util.setCatid(newsItem.getCatid());
            util.setImages(newsItem.getImages());
            util.setIntrotext(newsItem.getIntrotext());
            util.setAlias(newsItem.getAlias());
            util.setImageUrl(newsItem.getImageUrl());
            utilArrayList.add(util);

            //  Log.e("TAG", "OFFLINE DATA SIZE :" + utilArrayList.size());
        }

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (offline)
            menu.findItem(R.id.action_no_internet).setVisible(true);

        return true;
    }

    ArrayList<NewsUtil> utilArrayList = new ArrayList<NewsUtil>();

    private void serviceCallFornewsFeed(final StickyListHeadersListView viewList) {

        // Log.e("URL",Commons.BASE_URL + regId + "&e="+possibleEmail);
        JSONObject object = null;
        try {
            object = new JSONObject();
            object.put("reg_id", regId);
            object.put("e", possibleEmail);
        } catch (JSONException e) {

        }

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Commons.BASE_URL + "reg_id=" + regId + "&e=" + possibleEmail, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        setProgressBarIndeterminateVisibility(false);
                        progressBar.setVisibility(View.GONE);
                        if (response != null) {
                            //Log.e("TAG","Response : "+ response.toString());
                            utilArrayList.clear();
                            for (int count = 0; count < response.length(); count++) {
                                ArrayList<String> images = new ArrayList<String>();
                                try {
                                    NewsUtil util = new NewsUtil();
                                    JSONObject object = response.getJSONObject(count);

                                    util.setId(object.getString("id"));
                                    util.setTitle(object.getString("title"));
                                    util.setCatid(object.getString("catid"));
                                    util.setImages(object.getString("newsimage"));
                                    StringBuilder builder = new StringBuilder();
                                    JSONArray jsonArray = object.getJSONArray("paragraphs");
                                    for (int paracount = 0; paracount < jsonArray.length(); paracount++) {
                                        if (!jsonArray.getString(paracount).equals("")) {
                                            builder.append(jsonArray.getString(paracount)).append("\n");
                                        }

                                    }
                                    JSONArray imageArray = object.getJSONArray("image_url");
                                    //    Log.e("TAG", "Images Size : " + imageArray.length());
                                    for (int imagecount = 0; imagecount < imageArray.length(); imagecount++) {
                                        //     Log.e("TAG", "Images Array : " + imageArray.getString(imagecount));
                                        // if (!imageArray.getString(imagecount).equals("")) {
                                        images.add(Commons.URL + imageArray.getString(imagecount));

                                        //}
                                    }

                                    util.setImageUrl(images);
                                    // Log.e("TAG", "Images Under ImageUrl : " + util.getImageUrl());
                                    util.setIntrotext(builder.toString());//object.getString("newintrotext")
                                    //  util.setParagraphs(para);
                                    util.setAlias(object.getString("alias"));
                                    utilArrayList.add(util);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            NewsListAdapter adapter = new NewsListAdapter(NewsListActivity.this, utilArrayList, NewsListActivity.this);
                            viewList.setAdapter(adapter);
                            helper.deleteAllData(MPCDatabaseColumns.TABLE_NAME_ALL);
                            insertIntoDataBase(utilArrayList, MPCDatabaseColumns.TABLE_NAME_ALL);
                            offlineImageOnMenuBar(false);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setProgressBarIndeterminateVisibility(false);
                        //Log.e("TAG", "Error : " + error.getLocalizedMessage());
                        ArrayList<NewsUtil> utilArray = (ArrayList) helper.readAllData(MPCDatabaseColumns.TABLE_NAME_ALL);
                        NewsListAdapter adapter = new NewsListAdapter(NewsListActivity.this, utilArray, NewsListActivity.this);
                        viewList.setAdapter(adapter);
                        offlineImageOnMenuBar(true);
                        OfflineDataParsing();
                        //Toast.makeText(NewsListActivity.this, "No Data Obtained", Toast.LENGTH_LONG).show();
                    }


                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Log.e("URL ", jsonArrayRequest.getUrl());
        // Access the RequestQueue through your singleton class.
        VolleyHelper.getInstance(this).addToRequestQueue(jsonArrayRequest);

    }

    private void offlineImageOnMenuBar(boolean offlineValue) {
        offline = offlineValue;
        supportInvalidateOptionsMenu();
    }

    private void insertIntoDataBase(List<NewsUtil> imcModels, String tableName) {
        for (int i = 0; i < imcModels.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(MPCDatabaseColumns.COLUMN_ID, imcModels.get(i).getId());
            values.put(MPCDatabaseColumns.COLUMN_TITLE, imcModels.get(i).getTitle());
            values.put(MPCDatabaseColumns.COLUMN_ALIAS, imcModels.get(i).getAlias());
            values.put(MPCDatabaseColumns.COLUMN_IMAGE_URL, imcModels.get(i).getImages());
            values.put(MPCDatabaseColumns.COLUMN_CAT_ID, imcModels.get(i).getCatid());
            values.put(MPCDatabaseColumns.COLUMN_INTROTEXT, imcModels.get(i).getIntrotext());
            helper.insert(values, tableName);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.news_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//
//            return true;
//        }
//        if (id == R.id.action_refresh) {
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
        if (NetworkHelper.isOnline(NewsListActivity.this)) {
            if (itemPosition != 0) {
                Bundle bundle = new Bundle();
                // Log.e("TAG",utilArrayList.get(itemPosition).getCatid());
                bundle.putString("catId", utilArrayList.get(itemPosition).getCatid());

                loadActivity(NewsListActivity.this, MoreNewsActivity.class, bundle, 0);
            }
        } else {

        }
    }

    ArrayList<NewsUtil> newNews;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        newNews = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle.putString("catId", utilArrayList.get(position).getCatid());
        // Log.d("TAG", utilArrayList.get(position).getCatid());
        ArrayList<String> images = null;
        String catId = utilArrayList.get(position).getCatid();
        for (int count = 0; count < utilArrayList.size(); count++) {
            if (catId.equals(utilArrayList.get(count).getCatid())) {
                //  Log.d("TAG", "Count : " + count);
                //  List<String> imageSet = new ArrayList<>();
                // Log.e("TAG", "Images Under ImageUrl : " + utilArrayList.get(count).getImageUrl().size());
                //String newsImage = newUtilsData.get(count).getImages();
                images = utilArrayList.get(count).getImageUrl();
                //   Log.e("TAG", "Image url :" + Commons.URL + images.get(count));
                //  imageSet.add(0, newsImage);
                //if (images != null) {
                //   for (int imageCount = 0; count < images.size(); count++) {

                // Log.e("TAG", "Image url :" + Commons.URL + images.get(count));
                // imageSet.add(imageCount, Commons.URL + images.get(count));
                // }
                //  }
                NewsUtil newsUtil = new NewsUtil();
                newsUtil.setId(utilArrayList.get(count).getId());
                newsUtil.setTitle(utilArrayList.get(count).getTitle());
                newsUtil.setCatid(utilArrayList.get(count).getCatid());
                newsUtil.setImages(utilArrayList.get(count).getImages());
                // newsUtil.setImageUrl((ArrayList<String>) imageSet);
                newsUtil.setIntrotext(utilArrayList.get(count).getIntrotext());
                newsUtil.setAlias(utilArrayList.get(count).getAlias());
                newNews.add(newsUtil);
                // Log.e("TAG position : ", " " + position + " newNews :" + newNews.size() + "IMAGE ID : " + utilArrayList.get(count).getImages());
            }
//
        }
//        if (catId.equals("1")) {
//            position = position / 2;
//        }  if (catId.equals("2")) {
//            position = position / 3;
//        }  if (catId.equals("3")) {
//            position = position / 4;
//        }  if (catId.equals("11")) {
//            position = position / 5;
//        }  if (catId.equals("12")) {
//            position = position / 6;
//        }  if (catId.equals("13")) {
//            position = position / 7;
//        }
        //    Log.e("TAG position : ", " " + position + "  CAT ID :" + catId);
        bundle.putInt("position", position);
        bundle.putStringArrayList("imageArrayList", images);
        bundle.putParcelableArrayList("fullListData", newNews);
        loadActivity(NewsListActivity.this, NewsInSeriesActivity.class, bundle, 0);
    }

    /**
     * **** Commented Code On ItemClick
     * <p/>
     * bundle.putString("id", utilArrayList.get(position).getId());
     * bundle.putString("title", utilArrayList.get(position).getTitle());
     * bundle.putString("alias", utilArrayList.get(position).getAlias());
     * bundle.putString("newsContent", utilArrayList.get(position).getIntrotext());
     * bundle.putString("imageUrl", utilArrayList.get(position).getImages());
     * bundle.putStringArrayList("images", (ArrayList) utilArrayList.get(position).getImageUrl());
     */


    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {

    }

    @Override
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(this, getLocalClassName());
    }

    @Override
    protected void onStop() {
        super.onStop();
        VolleyHelper.getInstance(this).getRequestQueue().getCache().clear();
    }

    @Override
    public void itsABreakingNews(String breakingNews) {
        View view = LayoutInflater.from(this).inflate(R.layout.view_header_breaking_news, null);
        TextView lblBreakingNews = (TextView) view.findViewById(R.id.lblBreakingNews);
        lblBreakingNews.setText(breakingNews);
        viewList.addHeaderView(view);
    }
}
