package com.mpcnews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.mpcnews.R;
import com.mpcnews.adapter.PagerAdapter;
import com.mpcnews.fragments.ScreenSlidePageFragment;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.VolleyHelper;
import com.mpcnews.utils.NewsUtil;

import java.util.ArrayList;


public class NewsDetailsActivity extends BaseActivity {

    private String title, id, alias;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        initializeActionBar(true, true);
        Bundle bundle = getIntent().getBundleExtra("bundle");
        title = bundle.getString("title");
        id = bundle.getString("id");
        alias = bundle.getString("alias");
        String imageUrl = bundle.getString("imageUrl");
        String newsContent = bundle.getString("newsContent");
         //ArrayList<String> images = bundle.getStringArrayList("imageArrayList");
        // Log.e("TAG", bundle.getParcelableArrayList("fullListData") + "");
        //ArrayList<NewsUtil> newNews = bundle.getParcelableArrayList("fullListData");
       // Log.e("TAG", "Bhaiya ji SMile : " + images.get(1));
        // ArrayList<String> imageSet = new ArrayList<String>();
        //imageSet.add(0, imageUrl);

        // ViewPager newDetailsPage = (ViewPager) findViewById(R.id.newDetailsPage);

//        StringBuilder builder = new StringBuilder();
//        for (int count = 0 ;count<newsContent.size();count++) {
//            builder.append(newsContent.get(count)).append("\n");
//           // Log.e("TAG", "Builder Text :" + builder.toString());
//        }
        TextView lblNewsHeader = (TextView) findViewById(R.id.lblNewsHeader);
        TextView lblNewsContent = (TextView) findViewById(R.id.lblNewsContent);
        NetworkImageView imvNewsImage = (NetworkImageView) findViewById(R.id.imvNewsImage);

        // ViewPager pager = (ViewPager) findViewById(R.id.pager);

        //  Log.e("TAG", Commons.URL + newNews.get(co) + "");
//        if (newNews != null) {
//            for (int count = 0; count < images.size(); count++) {
//                imageSet.add(count + 1, Commons.URL + images.get(count));
//            }
//        }
//            PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager(), imageSet);
//            pager.setAdapter(mPagerAdapter);
//        } else {
//            pager.setVisibility(View.GONE);
//        }
        imvNewsImage.setImageUrl(imageUrl, VolleyHelper.getInstance(this).getImageLoader());
        lblNewsHeader.setText(title);
//        String stripped = newsContent.replaceAll("<[^>]*>", "");
        lblNewsContent.setText(newsContent);
        lblNewsContent.setMovementMethod(new ScrollingMovementMethod());

        FloatingActionButton action_share = (FloatingActionButton) findViewById(R.id.action_share);
        action_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareData();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.share_news, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
//            case R.id.action_share:
//
//                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareData() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, title + " " + "mpcnews.in/index.php/component/k2/item/" + id + "-" + alias);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(this, getLocalClassName());
    }
}
