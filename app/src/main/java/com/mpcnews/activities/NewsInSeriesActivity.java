package com.mpcnews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mpcnews.R;
import com.mpcnews.adapter.NewsSlideAdapter;
import com.mpcnews.adapter.PagerAdapter;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.utils.NewsUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by terril on 24/11/14.
 */
public class NewsInSeriesActivity extends BaseActivity {

    private int position;
    ArrayList<NewsUtil> newUtilsData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_series_in_news);

        initializeActionBar(true, true);

        Bundle bundle = getIntent().getBundleExtra("bundle");
        newUtilsData = bundle.getParcelableArrayList("fullListData");
        position = bundle.getInt("position");
        String catId = bundle.getString("catId");
        boolean moreNewsStatus = bundle.getBoolean("moreNews");
        ViewPager newDetailsPage = (ViewPager) findViewById(R.id.newDetailsPage);
        ArrayList<String> images = bundle.getStringArrayList("imageArrayList");
        // Log.e("TAG", bundle.getParcelableArrayList("fullListData") + "");
        //ArrayList<NewsUtil> newNews = bundle.getParcelableArrayList("fullListData");

        // Log.e("TAG", " NEW NEWS SIZE :" + newNews.size() + " POSITION : " + position + " Title : " + newNews.get(0).getTitle());
        NewsSlideAdapter slideAdapter = new NewsSlideAdapter(getSupportFragmentManager(), newUtilsData, images);
        newDetailsPage.setAdapter(slideAdapter);
        if (moreNewsStatus) {
            newDetailsPage.setCurrentItem(position);
        } else {
            position = getPosition(position);
            newDetailsPage.setCurrentItem(position);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(this, this.getLocalClassName());
    }

    private int getPosition(int position) {

        int pagerPosition = position % 5;

        // Log.e("TAG" , pagerPosition + " News in Series News");
        //Log.d("TAG", "" + pagerPosition);
        return pagerPosition;
    }
}


