package com.mpcnews.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.NetworkImageView;
import com.mpcnews.R;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.VolleyHelper;

import java.util.ArrayList;

/**
 * Created by terril on 3/11/14.
 */
public class ScreenSlidePageFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_pager_images, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        NetworkImageView imvNewsImage = (NetworkImageView) view.findViewById(R.id.imvNewsImage);
        ImageView imvArrow = (ImageView) view.findViewById(R.id.imvArrow);
        String imageUrl = bundle.getString("imageUrl");
        boolean nextItem = bundle.getBoolean("nextItem");
        // Log.e("TAG", nextItem + "");
        if (nextItem)
            imvArrow.setVisibility(View.VISIBLE);
        else
            imvArrow.setVisibility(View.INVISIBLE);
        imvNewsImage.setImageUrl(imageUrl, VolleyHelper.getInstance(getActivity()).getImageLoader());

    }

    @Override
    public void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(getActivity() , getClass().getSimpleName());
    }
}
