package com.mpcnews.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.toolbox.NetworkImageView;
import com.mpcnews.R;
import com.mpcnews.helpers.VolleyHelper;

/**
 * Created by terril1 on 22/12/15.
 */
public class ShowImageFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_show_image, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        String imageUrl = bundle.getString("imageUrl");
        NetworkImageView imvZoomedImage = (NetworkImageView) view.findViewById(R.id.imvZoomedImage);
        imvZoomedImage.setImageUrl(imageUrl, VolleyHelper.getInstance(getActivity()).getImageLoader());
    }

}
