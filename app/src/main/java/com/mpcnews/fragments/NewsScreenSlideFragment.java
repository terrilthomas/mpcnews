package com.mpcnews.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.mpcnews.R;
import com.mpcnews.activities.SettingsActivity;
import com.mpcnews.adapter.ImageAdapter;
import com.mpcnews.adapter.NewsSlideAdapter;
import com.mpcnews.adapter.PagerAdapter;
import com.mpcnews.helpers.AnalyticsTrackerHelper;
import com.mpcnews.helpers.Commons;
import com.mpcnews.helpers.VolleyHelper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by terril on 24/11/14.
 */
public class NewsScreenSlideFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private String title, alias, id;
    ArrayList<String> imageList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_news_slide, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        TextView lblNewsHeader = (TextView) view.findViewById(R.id.lblNewsHeader);
        TextView lblNewsContent = (TextView) view.findViewById(R.id.lblNewsContent);
        NetworkImageView imvNewsImageHeader = (NetworkImageView) view.findViewById(R.id.imvNewsImageHeader);
        GridView grdImages = (GridView) view.findViewById(R.id.grdImages);
        // ViewPager pager = (ViewPager) view.findViewById(R.id.pager);

        String newsHeader = bundle.getString("newsHeader");
        String newsContent = bundle.getString("newsContent");
        alias = bundle.getString("alias");
        id = bundle.getString("id");
        String imageUrl = bundle.getString("imageUrl");
        imageList = bundle.getStringArrayList("newsImageUrl");

        //Log.e("TAG", "Bhaiya ji SMile : " + imageList.size());

        imvNewsImageHeader.setImageUrl(imageUrl, VolleyHelper.getInstance(getActivity()).getImageLoader());
        ImageAdapter adapter = new ImageAdapter(getActivity(), imageList);
        grdImages.setAdapter(adapter);
        grdImages.setOnItemClickListener(this);
        // ArrayList<String> images = bundle.getStringArrayList("newsImageUrl");

        // Log.e("TAG", images + "");
//        if (images.size() > 0) {
//            PagerAdapter mPagerAdapter = new PagerAdapter(getChildFragmentManager(), images);
//            pager.setAdapter(mPagerAdapter);
//        } else {
//            pager.setVisibility(View.GONE);
//        }

        title = newsHeader;
        lblNewsHeader.setText(newsHeader);
        lblNewsContent.setText(newsContent);
        lblNewsContent.setMovementMethod(new ScrollingMovementMethod());


        FloatingActionButton action_share = (FloatingActionButton) view.findViewById(R.id.action_share);
        action_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareData();
            }
        });
//        ArrayList<String> images = bundle.getStringArrayList("images");
//        Log.e("TAG", bundle.getParcelableArrayList("fullListData") + ""); // bundle.getParcelableArrayList("fullListData");
//        ArrayList<String> imageSet = new ArrayList<String>();
        // Log.e("TAG", images + "");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.share_news, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                shareData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareData() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, title + " " + "mpcnews.in/index.php/component/k2/item/" + id + "-" + alias);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share"));
    }

    @Override
    public void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(getActivity(), getClass().getSimpleName());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Bundle bundle = new Bundle();
//        bundle.putString("imageUrl", imageList.get(position));
//        // FragmentManager fm = getFragmentManager();
//        Fragment newFragment = new ShowImageFragment();
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        newFragment.setArguments(bundle);
//        transaction.replace(R.id.fragmentContianer, newFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();

        loadPhoto(imageList.get(position));
//        new AsyncTask<Void, Void, Bitmap>() {
//
//            @Override
//            protected Bitmap doInBackground(Void[] params) {
//                URL url = null;
//                Bitmap image = null;
//                try {
//                    url = new URL(imageList.get(position));
//                    image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                return image;
//            }
//
//            @Override
//            protected void onPostExecute(Bitmap image) {
//                super.onPostExecute(image);
//                showImageinDialog(image);
//            }
//        };

        //newFragment.show(fm, null);
    }


//    private void showImageinDialog(Bitmap imageBitmap) {
//        Dialog dialog = new Dialog(getActivity());
//        dialog.setContentView(R.layout.fragment_show_image);
//        dialog.setCancelable(true);
//        //there are a lot of settings, for dialog, check them all out!
//
//        NetworkImageView imvZoomedImage = (NetworkImageView) dialog.findViewById(R.id.imvZoomedImage);
//        imvZoomedImage.setImageBitmap(imageBitmap);
//
//
//        //now that the dialog is set up, it's time to show it
//        dialog.show();
//
//    }

    private void loadPhoto(String imageUrl) {

        // ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.fragment_show_image,
                null);
        NetworkImageView imvZoomedImage = (NetworkImageView) layout.findViewById(R.id.imvZoomedImage);
        imvZoomedImage.setImageUrl(imageUrl, VolleyHelper.getInstance(getActivity()).getImageLoader());
        imageDialog.setView(layout);
//        imageDialog.setPositiveButton(resources.getString(R.string.ok_button), new DialogInterface.OnClickListener(){
//
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//
//        });


        imageDialog.create();
        imageDialog.show();
    }
}
