package com.mpcnews.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mpcnews.R;
import com.mpcnews.helpers.AnalyticsTrackerHelper;

/**
 * Created by terril on 13/10/14.
 */
public class AboutFragment extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView lblUrl = (TextView) view.findViewById(R.id.lblUrl);
        lblUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://mpcnews.in")));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        AnalyticsTrackerHelper.trackUserCurrentPage(getActivity(), getClass().getSimpleName());
    }
}
